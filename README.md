# About avc-binding-json

This is very comparable
to [avc-binding-dom](https://gitlab.com/avcompris/avc-binding-dom/), but for
JSON rather than for XML.

Its parent project is [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/).

Project dependencies include:

  * [avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)
  * [avc-commons-testutil](https://gitlab.com/avcompris/avc-commons-testutil/)
  * [avc-binding-common](https://gitlab.com/avcompris/avc-binding-common/)

This is the project home page, hosted on GitLab.


[API Documentation is here](https://maven.avcompris.com/avc-binding-json/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-binding-json/)
