package net.avcompris.binding.json;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.Binder;

/**
 * a Binder is responsible for binding a Java dynamix proxy to a JSON incoming document.
 *
 * @author David Andrianavalontsalama
 */
public interface JsonBinder extends Binder<Object> {

	/**
	 * bind a Java dynamic proxy to a JSON incoming document,
	 * using the XPath expression declared as an annotation in the interface.
	 */
	@Override
	<T> T bind(Object json, Class<T> clazz);

	/**
	 * bind a Java dynamic proxy to a JSON incoming document,
	 * using the XPath expression declared as an annotation in the interface
	 * and the {@link ClassLoader} passed as a parameter.	 
	 */
	@Override
	<T> T bind(Object json, ClassLoader classLoader, Class<T> clazz);

	/**
	 * bind a Java dynamic proxy to a JSON incoming document,
	 * using the XPath expression passed as a parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, Object json, Class<T> clazz);

	/**
	 * bind a Java dynamic proxy to a JSON incoming document,
	 * using the XPath expression
	 * and the {@link ClassLoader} passed as a parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, Object json,
			ClassLoader classLoader, Class<T> clazz);
}
