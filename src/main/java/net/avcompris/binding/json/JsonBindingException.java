package net.avcompris.binding.json;

import net.avcompris.binding.BindingException;

import com.avcompris.common.annotation.Nullable;

/**
 * This exception will be thrown when a call to a method bound
 * should encounter an error due to an XPath expression. 
 */
public class JsonBindingException extends BindingException {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = 5879017758141169978L;

	/**
	 * constructor.
	 */
	public JsonBindingException(
			@Nullable final String xpath,
			@Nullable final Throwable cause) {

		super(xpath, cause);
	}
}
