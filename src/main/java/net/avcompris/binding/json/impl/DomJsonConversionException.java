package net.avcompris.binding.json.impl;

import com.avcompris.common.annotation.Nullable;

/**
 * this exception is thrown when a JSON incoming document could not be converted
 * to a DOM node, due to illegal XML names for instance.
 * 
 * @author David Andrianavalontsalama
 */
public class DomJsonConversionException extends RuntimeException {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = 2138509094329228593L;

	/**
	 * constructor.
	 */
	public DomJsonConversionException(@Nullable final String message,
			@Nullable final Throwable cause) {

		super(message, cause);
	}
}
