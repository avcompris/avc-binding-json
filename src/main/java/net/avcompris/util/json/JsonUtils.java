package net.avcompris.util.json;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.leftPad;
import static org.apache.commons.lang3.StringUtils.split;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.uncapitalize;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Nullable;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.avcompris.lang.NotImplementedException;
import com.avcompris.util.AbstractUtils;
import com.avcompris.util.AvcXMLSerializer;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.json.impl.DomJsonConverter;

/**
 * utility class for JSON manipulation.
 * 
 * @author David Andrianavalontsalama Copyright Avantage Compris SARL 2013 ©
 */
public abstract class JsonUtils extends AbstractUtils {

	/**
	 * retrieve a property from a JSON map.
	 * 
	 * @param <T>
	 *            the class that represents the <code>type</code> passed as parameter
	 * @param type
	 *            the required type
	 * @param json
	 *            the JSON object (a map)
	 * @param propertyName
	 *            the property's name
	 * @return the property's value
	 */
	public static <T> T getProperty(final Class<T> type, final JSONObject json, final String propertyName) {

		nonNullArgument(type, "type");
		nonNullArgument(json, "json");
		nonNullArgument(propertyName, "propertyName");

		final Map<?, ?> map = json;

		final Object value = map.get(propertyName);

		if (value == null) {
			return null;
		}

		if (!type.isInstance(value)) {

			if (type.equals(String.class)) {
				return type.cast(value.toString());
			}

			throw new ClassCastException("Property value for \"" + propertyName + "\" is not an instance of "
					+ type.getName() + ": " + value.getClass().getName());
		}

		return type.cast(value);
	}

	/**
	 * retrieve the property names from a JSON map.
	 * 
	 * @param json
	 *            the JSON object (a map)
	 * @return the property names
	 */
	public static String[] getPropertyNames(final JSONObject json) {

		nonNullArgument(json, "json");

		final Map<?, ?> map = json;

		final String[] names = new String[map.size()];

		int i = 0;

		for (final Object key : map.keySet()) {

			if (!(key instanceof String)) {
				throw new ClassCastException("Property name #" + i + " is not an instance of String: " + key);
			}

			names[i] = (String) key;

			++i;
		}

		return names;
	}

	/**
	 * retrieve a {@link String} property from a JSON map.
	 * 
	 * @param json
	 *            the JSON object (a map)
	 * @param propertyName
	 *            the property's name
	 * @return the property's value
	 */
	public static String getStringProperty(final JSONObject json, final String propertyName) {

		return getProperty(String.class, json, propertyName);
	}

	/**
	 * retrieve a {@link Boolean} property from a JSON map.
	 * 
	 * @param json
	 *            the JSON object (a map)
	 * @param propertyName
	 *            the property's name
	 * @return the property's value
	 */
	public static Boolean getBooleanProperty(final JSONObject json, final String propertyName) {

		return getProperty(Boolean.class, json, propertyName);
	}

	/**
	 * retrieve a {@link List} property from a JSON map.
	 * 
	 * @param json
	 *            the JSON object (a map)
	 * @param propertyName
	 *            the property's name
	 * @return the property's value
	 */
	public static Collection<?> getListProperty(final JSONObject json, final String propertyName) {

		return getProperty(List.class, json, propertyName);
	}

	/**
	 * retrieve a {@link Object} property from a JSON map.
	 * 
	 * @param json
	 *            the JSON object (a map)
	 * @param propertyName
	 *            the property's name
	 * @return the property's value
	 */
	public static Object getObjectProperty(final JSONObject json, final String propertyName) {

		return getProperty(Object.class, json, propertyName);
	}

	public static void printJSON2YAML(final PrintWriter pw, final String rootElement, final Map<?, ?> map)
			throws IOException {

		nonNullArgument(pw, "pw");
		nonNullArgument(rootElement, "rootElement");
		nonNullArgument(map, "map");

		pw.println(rootElement + ':');

		JsonUtils.printJSON2YAML(pw, "    ", false, map);
	}

	private static String escape(final String s) {

		final StringBuilder sb = new StringBuilder();

		for (final char c : s.toCharArray()) {

			if (c <= 255) {
				sb.append(c);
			} else {
				sb.append("\\u" + leftPad(Integer.toHexString(c), 4, '0'));
			}
		}

		return sb.toString();
	}

	private static void printJSON2YAML(final PrintWriter pw, final String indent, final boolean isListItem,
			final Map<?, ?> map) throws IOException {

		final Set<Object> orderedKeys = new TreeSet<Object>();

		orderedKeys.addAll(map.keySet());

		boolean start = true;

		// 1. SIMPLE-VALUE ATTRIBUTES

		for (final Object k : orderedKeys) {

			final String key = (String) k;

			final Object v = map.get(key);

			if (v == null) {
				continue;
			}

			if (!isSimpleValue(v)) {
				continue;
			}

			final String prefix;

			if (start) {
				prefix = isListItem ? "- " : "";
				start = false;
			} else {
				prefix = isListItem ? "  " : "";
			}

			final String value = v.toString();

			if (value.contains("\"")) {

				pw.println(indent + prefix + key + ": |");
				for (final String slice : split(value, "\r\n")) {
					pw.println(indent + "    " + escape(slice));
				}

			} else {

				pw.println(indent + prefix + key + ": " + escape(value));
			}
		}

		// 2. SUB-STRUCTURE ATTRIBUTES

		for (final Object k : orderedKeys) {

			final String key = (String) k;

			final Object value = map.get(key);

			if (value == null) {
				continue;
			}

			if (isSimpleValue(value)) {
				continue;
			}
			if (Map.class.isInstance(value) && ((Map<?, ?>) value).isEmpty()) {
				continue;
			}
			if (List.class.isInstance(value) && ((List<?>) value).isEmpty()) {
				continue;
			}

			final String prefix;

			if (start) {
				prefix = isListItem ? "- " : "";
				start = false;
			} else {
				prefix = isListItem ? "  " : "";
			}

			pw.println(indent + prefix + key + ":");

			if (Map.class.isInstance(value)) {

				printJSON2YAML(pw, indent + "    ", false, (Map<?, ?>) value);

			} else {

				for (final Object item : (List<?>) value) {

					if (isSimpleValue(item)) {

						throw new NotImplementedException("item.class: " + item.getClass().getName());

					} else if (Map.class.isInstance(item)) {

						printJSON2YAML(pw, indent + prefix + "  ", true, (Map<?, ?>) item);

					} else {

						final List<?> list = (List<?>) item;

						boolean onlySimpleValues = true;

						for (final Object item2 : list) {

							if (!isSimpleValue(item2)) {
								onlySimpleValues = false;
								break;
							}
						}

						if (onlySimpleValues) {

							pw.println(indent + "  - [" //
									+ join(list, ", ") + "]");

							continue;
						}

						boolean start2 = true;

						for (final Object item2 : list) {

							if (isSimpleValue(item2)) {

								pw.println(indent + "  " + (start2 ? "- " : "  ") + ("- " + item2));

							} else if (Map.class.isInstance(item2)) {

								printJSON2YAML(pw, indent + "  - ", true, (Map<?, ?>) item);

							} else {

								throw new NotImplementedException("item2.class: " + item.getClass().getName());

							}

							if (start2) {
								start2 = false;
							}
						}
					}
				}
			}
		}
	}

	public static boolean isSimpleValue(final Object value) {

		checkNotNull(value, "value");

		if (Map.class.isInstance(value)) {

			return false;

		}
		if (List.class.isInstance(value)) {

			return false;

		} else if (String.class.isInstance(value) || Integer.class.isInstance(value) || Short.class.isInstance(value)
				|| Long.class.isInstance(value) || Float.class.isInstance(value) || Double.class.isInstance(value)
				|| Boolean.class.isInstance(value) || Byte.class.isInstance(value) || Character.class.isInstance(value)
				|| value.getClass().isPrimitive()) {

			return true;

		} else {

			throw new NotImplementedException("Unknown value.type: " + value.getClass().getName());
		}
	}

	public static void printJSON2XML(final Object json, final ContentHandler contentHandler)
			throws IOException, ParserConfigurationException, SAXException {

		nonNullArgument(contentHandler, "contentHandler");
		nonNullArgument(json, "json");

		final Document document = DomJsonConverter.jsonToDocument(json, BindConfiguration.newBuilder().build());

		serialize(document.getDocumentElement(), contentHandler);
	}

	private static void serialize(final Node node, final ContentHandler contentHandler) throws SAXException {

		nonNullArgument(node, "node");
		nonNullArgument(contentHandler, "contentHandler");

		switch (node.getNodeType()) {

		case Node.ELEMENT_NODE:

			final NamedNodeMap domAttributes = node.getAttributes();

			final AttributesImpl attributes = new AttributesImpl();

			for (int i = 0; i < domAttributes.getLength(); ++i) {

				final Node domAttribute = domAttributes.item(i);

				final String n = domAttribute.getNodeName();

				attributes.addAttribute(null, n, n, "CDATA", domAttribute.getNodeValue());
			}

			@Nullable
			final String namespaceURI = node.getNamespaceURI();
			@Nullable
			final String localName = node.getLocalName();
			final String nodeName = node.getNodeName();

			@Nullable
			final String qName = namespaceURI == null ? null : nodeName;

			contentHandler.startElement(namespaceURI, namespaceURI == null ? nodeName : localName, qName, attributes);

			final NodeList children = node.getChildNodes();

			for (int i = 0; i < children.getLength(); ++i) {

				final Node child = children.item(i);

				serialize(child, contentHandler);
			}

			contentHandler.endElement(namespaceURI, nodeName, qName);

			break;

		case Node.TEXT_NODE:

			final char[] chars = node.getTextContent().toCharArray();

			contentHandler.characters(chars, 0, chars.length);

			break;

		default:

			throw new NotImplementedException("nodeType: " + node.getNodeType());
		}
	}

	public static void printJSON2XML(final Object json, final PrintWriter pw)
			throws IOException, ParserConfigurationException, SAXException {

		nonNullArgument(pw, "pw");
		nonNullArgument(json, "json");

		printJSON2XML(json, new AvcXMLSerializer(pw));
	}

	/**
	 * 
	 * @param jsonFile
	 * @param clazz
	 *            must be an interface
	 */
	public static <T> T parseJSON(final File jsonFile, final Class<T> clazz) throws IOException, ParseException {

		checkNotNull(jsonFile, "jsonFile");
		checkNotNull(clazz, "clazz");

		final String json = FileUtils.readFileToString(jsonFile, UTF_8);

		return parseJSON(json, clazz);
	}

	/**
	 * 
	 * @param json
	 * @param clazz
	 *            must be an interface
	 */
	public static <T> T parseJSON(final String json, final Class<T> clazz) throws ParseException {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		final JSONParser parser = new JSONParser();

		return parseJSON(parser.parse(json), clazz);
	}

	public static <T> T parseJSON(final Object json, final Class<T> clazz) throws ParseException {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		if (clazz.isArray()) {

			final Class<?> componentType = clazz.getComponentType();

			final JSONArray jsonArray = (JSONArray) json;

			final int length = jsonArray.size();

			final Object array = Array.newInstance(componentType, length);

			for (int i = 0; i < length; ++i) {

				Array.set(array, i, parseJSON((JSONObject) jsonArray.get(i), componentType));
			}

			return clazz.cast(array);

		} else {

			return parseJSON((JSONObject) json, clazz);
		}
	}

	/**
	 * 
	 * @param json
	 * @param clazz
	 *            must be an interface
	 */
	public static <T> T parseJSON(final JSONObject json, final Class<T> clazz) throws ParseException {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		if (!clazz.isInterface()) {
			throw new IllegalArgumentException("clazz should be an interface, but was: " + clazz.getName());
		}

		final Object proxy = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
				new Class<?>[] { clazz }, new InvocationHandler() {

					@Override
					public Object invoke(final Object proxy, final Method method, @Nullable final Object[] args)
							throws Throwable {

						checkNotNull(proxy, "proxy");
						checkNotNull(method, "method");

						final String methodName = method.getName();

						final Class<?>[] paramTypes = method.getParameterTypes();

						final boolean hasNoParam = (paramTypes == null) || (paramTypes.length == 0);
						final boolean hasOneParam = (paramTypes != null) && (paramTypes.length == 1);

						if (hasOneParam && "equals".equals(methodName)) {

							return JsonUtils.equals(clazz, json, args[0]);
						}

						if (!hasNoParam) {
							throw new NotImplementedException(
									"class: " + clazz.getSimpleName() + ", method: " + methodName);
						}

						final Class<?> returnType = method.getReturnType();

						final String propertyName;

						if ("toString".equals(methodName)) {

							return JsonUtils.toString(clazz, json);

						} else if ("getClass".equals(methodName)) {

							return proxy.getClass();

						} else if ("hashCode".equals(methodName)) {

							return JsonUtils.hashCode(clazz, json);

						} else if (methodName.startsWith("get")) {
							propertyName = uncapitalize(substringAfter(methodName, "get"));
						} else if (methodName.startsWith("is")) {
							propertyName = uncapitalize(substringAfter(methodName, "is"));
						} else {
							throw new NotImplementedException(
									"class: " + clazz.getSimpleName() + ", method: " + methodName);
						}

						final Object value = ((JSONObject) json).get(propertyName);

						if (returnType.isArray()) {

							if (value == null) {

								return null;

							} else if (byte[].class.equals(returnType)) {

								return Base64.getDecoder().decode(value.toString());

							} else {

								final Class<?> componentType = returnType.getComponentType();

								final JSONArray jsonArray = (JSONArray) value;

								final int length = jsonArray.size();

								final Object array = Array.newInstance(componentType, length);

								for (int i = 0; i < length; ++i) {

									Array.set(array, i, parseJSON((JSONObject) jsonArray.get(i), componentType));
								}

								return array;
							}

						} else if (Integer.class.equals(returnType)) {

							return (value == null) ? null : ((Long) value).intValue();

						} else if (int.class.equals(returnType)) {

							return ((Long) value).intValue();

						} else if (Short.class.equals(returnType)) {

							return (value == null) ? null : ((Long) value).shortValue();

						} else if (short.class.equals(returnType)) {

							return ((Long) value).shortValue();

						} else if (Long.class.equals(returnType)) {

							return value;

						} else if (long.class.equals(returnType)) {

							return ((Long) value).longValue();

						} else if (Double.class.equals(returnType)) {

							return value;

						} else if (double.class.equals(returnType)) {

							return ((Double) value).doubleValue();

						} else if (Float.class.equals(returnType)) {

							return (value == null) ? null : ((Double) value).floatValue();

						} else if (float.class.equals(returnType)) {

							return ((Double) value).floatValue();

						} else if (Boolean.class.equals(returnType)) {

							return value;

						} else if (boolean.class.equals(returnType)) {

							return ((Boolean) value).booleanValue();

						} else if (value instanceof JSONObject) {

							return parseJSON((JSONObject) value, returnType);

						} else if (returnType.isEnum()) {

							if (value == null) {

								return null;
							}

							for (final Object enumConstant : returnType.getEnumConstants()) {

								if (value.equals(enumConstant.toString())) {

									return enumConstant;
								}
							}

							throw new NotImplementedException(
									"enum value: " + value + ", for class: " + returnType.getName());

						} else {

							return returnType.cast(value);
						}
					}
				});

		return clazz.cast(proxy);
	}

	private static Map<String, Method> getGetters(final Class<?> clazz) {

		checkNotNull(clazz, "clazz");

		// Extract all property names

		final Map<String, Method> getters = new HashMap<String, Method>();

		for (final Method method : clazz.getMethods()) {

			final String methodName = method.getName();

			final Class<?>[] paramTypes = method.getParameterTypes();

			final boolean hasNoParam = (paramTypes == null) || (paramTypes.length == 0);

			if (!hasNoParam) {
				continue;
			}

			if ("getClass".equals(methodName)) {
				continue;
			}

			// final Class<?> returnType = method.getReturnType();

			final String propertyName;

			if (methodName.startsWith("get")) {
				propertyName = uncapitalize(substringAfter(methodName, "get"));
			} else if (methodName.startsWith("is")) {
				propertyName = uncapitalize(substringAfter(methodName, "is"));
			} else {
				continue;
			}

			getters.put(propertyName, method);
		}

		return getters;
	}

	private static String toString(final Class<?> clazz, final JSONObject json) {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		final Map<String, Method> getters = getGetters(clazz);

		// Extract property names, sorted

		final StringBuilder sb = new StringBuilder("{");

		boolean start = true;

		for (final String propertyName : new TreeSet<String>(getters.keySet())) {

			final Class<?> returnType = getters.get(propertyName).getReturnType();

			final Object value = json.get(propertyName);

			if (value == null) {
				continue;
			}

			if (start) {
				start = false;
			} else {
				sb.append(',');
			}

			sb.append('"').append(propertyName).append("\":");

			if (returnType.isArray()) {

				sb.append('[');

				final Class<?> componentType = returnType.getComponentType();

				final JSONArray jsonArray = (JSONArray) value;

				final int length = jsonArray.size();

				for (int i = 0; i < length; ++i) {

					if (i != 0) {
						sb.append(',');
					}

					sb.append(JsonUtils.toString(componentType, (JSONObject) jsonArray.get(i)));
				}

				sb.append(']');

			} else if (int.class.equals(returnType)) {
				sb.append(value);
			} else if (boolean.class.equals(returnType)) {
				sb.append(value);
			} else if (String.class.equals(returnType)) {
				sb.append('"').append(value.toString().replace("\"", "\\\"").replace("\\", "\\\\")).append('"');
			} else {
				throw new NotImplementedException("class: " + returnType.getName() + " (" + value + ")");
			}
		}

		sb.append("}");

		return sb.toString();
	}

	private static int hashCode(final Class<?> clazz, final JSONObject json) {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		final Map<String, Method> getters = getGetters(clazz);

		int hashCode = 0;

		for (final String propertyName : getters.keySet()) {

			hashCode += propertyName.hashCode();

			final Object value = json.get(propertyName);

			if (value != null) {

				hashCode += value.hashCode();
			}
		}

		return hashCode;
	}

	private static boolean equals(final Class<?> clazz, final JSONObject json, @Nullable final Object arg)
			throws InvocationTargetException, IllegalAccessException, IllegalArgumentException {

		checkNotNull(json, "json");
		checkNotNull(clazz, "clazz");

		if (arg == null) {
			return false;
		}

		final Map<String, Method> getters0 = getGetters(clazz);
		final Map<String, Method> getters1 = getGetters(arg.getClass());

		for (final String propertyName : getters0.keySet()) {

			if (!getters1.containsKey(propertyName)) {
				System.out.println("x1 " + propertyName);
				return false;
			}

			final Object jsonValue = json.get(propertyName);
			final Object value1 = getters1.get(propertyName).invoke(arg);

			if (jsonValue == null && value1 == null) {
				continue;
			} else if (jsonValue == null || value1 == null) {
				System.out.println("x2 " + propertyName);
				return false;
			}

			final Object value0;

			final Class<?> returnType = getters0.get(propertyName).getReturnType();
			final Class<?> returnType1 = getters0.get(propertyName).getReturnType();

			if (!returnType.equals(returnType1)) {
				return false;
			}

			if (returnType.isArray()) {

				final Class<?> componentType = returnType.getComponentType();

				final JSONArray jsonArray = (JSONArray) jsonValue;

				final int length = jsonArray.size();

				for (int i = 0; i < length; ++i) {

					if (!JsonUtils.equals(componentType, (JSONObject) jsonArray.get(i), Array.get(value1, i))) {
						return false;
					}
				}

				continue;

			} else if (int.class.equals(returnType)) {
				value0 = ((Long) jsonValue).intValue();
			} else if (boolean.class.equals(returnType)) {
				value0 = ((Boolean) jsonValue).booleanValue();
			} else if (String.class.equals(returnType)) {
				value0 = jsonValue.toString();
			} else {
				throw new NotImplementedException(
						"jsonValue: " + jsonValue.getClass().getName() + " (" + jsonValue + ")");
			}

			if (!value0.equals(value1)) {
				System.out.println("x3 " + propertyName + ", " + value0 + " (" + value0.getClass() + ")" + ", " + value1
						+ " (" + value1.getClass() + ")");
				return false;
			}
		}

		for (final String propertyName : getters1.keySet()) {

			if (!getters0.containsKey(propertyName)) {
				System.out.println("x4 " + propertyName);
				return false;
			}

			// No need to compare values a second time, property names suffice
		}

		return true;
	}
}
