package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

public class ArraysJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Before
	public void setUp() throws Exception {

		final Object json = parse(new File("src/test/json/010-arrays.json"));

		final JsonBinder binder = new DomJsonBinder();

		arrays = binder.bind(json, MyArrays.class);
	}

	private MyArrays arrays;

	@Test
	public void test_40_0_40_41() {

		assertEquals(2, arrays.getControlPath().length);

		assertEquals(0, arrays.getControlPath()[0].getX());
		assertEquals(0, arrays.getControlPath()[0].getY());

		assertEquals(4, arrays.sizeOfItems());

		assertEquals(0, arrays.getItems()[0]);
		assertEquals(0, arrays.getItems()[1]);
		assertEquals(40, arrays.getItems()[2]);
		assertEquals(-41, arrays.getItems()[3]);

		assertEquals(40, arrays.getControlPath()[1].getX());
		assertEquals(-41, arrays.getControlPath()[1].getY());
	}

	@Test
	public void testSimpleInts() {

		assertEquals(3, arrays.getSimpleInts().length);
		assertEquals(45, arrays.getSimpleInts()[0]);
		assertEquals(89, arrays.getSimpleInts()[1]);
		assertEquals(23, arrays.getSimpleInts()[2]);
	}

	@XPath("/json")
	@Namespaces("xmlns:json=http://json.org/")
	public interface MyArrays {

		@XPath("simple_ints")
		int[] getSimpleInts();

		@XPath("controlPath")
		ControlPath[] getControlPath();

		@XPath("//json:item")
		int[] getItems();

		int sizeOfItems();

		interface ControlPath {

			@XPath("json:item[1]")
			int getX();

			@XPath("json:item[2]")
			int getY();
		}
	}
}
