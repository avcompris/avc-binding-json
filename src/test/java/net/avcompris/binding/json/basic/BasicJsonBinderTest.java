package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Test;

public class BasicJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Test
	public void testBasic() throws Exception {

		final Object json = parse(new File(
				"src/test/json/001-graph_facebook_com_10151573527384700.js"));

		final JsonBinder binder = new DomJsonBinder();

		final FacebookGraph graph = binder.bind(json, FacebookGraph.class);

		assertEquals("10151573527384700", graph.getId());

		assertEquals("99206759699", graph.getFromId());

		assertEquals(435, graph.getHeight());
	}

	@Test
	public void testList() throws Exception {

		final Object json = parse(new File(
				"src/test/json/001-graph_facebook_com_10151573527384700.js"));

		final JsonBinder binder = new DomJsonBinder();

		final FacebookGraph graph = binder.bind(json, FacebookGraph.class);

		assertEquals(9, graph.sizeOfImages());

		assertEquals(9, graph.getImages().length);

		assertEquals(774, graph.getImages()[0].getHeight());

		assertEquals(580, graph.getImages()[1].getHeight());
	}

	@XPath("/*")
	private interface FacebookGraph {

		@XPath("@id")
		String getId();

		@XPath("from/@id")
		String getFromId();

		@XPath("@height")
		int getHeight();

		@XPath("images")
		Image[] getImages();

		int sizeOfImages();

		interface Image {

			@XPath("@height")
			int getHeight();
		}
	}
}
