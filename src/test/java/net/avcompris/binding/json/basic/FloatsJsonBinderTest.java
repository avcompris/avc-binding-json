package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

public class FloatsJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Before
	public void setUp() throws Exception {

		final Object json = parse(new File("src/test/json/009-floats.json"));

		final JsonBinder binder = new DomJsonBinder();

		floats = binder.bind(json, Floats.class);
	}

	private Floats floats;

	@Test
	public void test_236_5() {

		assertEquals(236.5, floats.getA(), 0.0001);
	}

	@XPath("/json")
	public interface Floats {

		@XPath("@a")
		float getA();
	}
}
