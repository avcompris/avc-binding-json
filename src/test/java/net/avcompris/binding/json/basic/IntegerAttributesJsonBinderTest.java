package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

public class IntegerAttributesJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Before
	public void setUp() throws Exception {

		final Object json = parse(new File(
				"src/test/json/008-integer-attributes.json"));

		final JsonBinder binder = new DomJsonBinder();

		integerAttributes = binder.bind(json, IntegerAttributes.class);
	}

	private IntegerAttributes integerAttributes;

	@Test
	public void test_1Value() {

		assertEquals("Hello", integerAttributes.getOne());
	}

	@XPath("/json")
	@Namespaces("xmlns:json=http://json.org/")
	public interface IntegerAttributes {

		@XPath("json:attribute[@name = '1']/@value")
		String getOne();
	}
}
