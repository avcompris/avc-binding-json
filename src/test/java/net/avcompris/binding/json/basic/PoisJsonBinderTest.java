package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.basic.PoisJsonBinderTest.Pois.Poi;
import net.avcompris.binding.json.basic.PoisJsonBinderTest.Pois.PoiGroup;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

import com.avcompris.common.annotation.Nullable;

public class PoisJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Before
	public void setUp() throws Exception {

		final Object json = parse(new File("src/test/json/006-pois.json"));

		final JsonBinder binder = new DomJsonBinder();

		pois = binder.bind(json, Pois.class);
	}

	private Pois pois;

	@Test
	public void test_emailsAreAllNull() {

		for (final PoiGroup group : pois.getPois()) {

			for (final Poi poi : group.getPois()) {

				assertNull(poi.getEmail());
			}
		}
	}

	@Test
	public void test_emailsIsNull() {

		assertNull(pois.getEmails());
	}

	@Test
	public void test_coordinatesAsDouble() {

		// lng":"2.0720601081848","imageWidth":0,"lat":"49.03312721008

		final Poi poi3792 = pois.getPois()[1].getPois()[0];

		assertEquals(49.03312721008, poi3792.getLatAsDouble(), 0.0);
		assertEquals(2.0720601081848, poi3792.getLngAsDouble(), 0.0);
	}

	@XPath("/*")
	public interface Pois {

		@XPath("groups")
		PoiGroup[] getPois();

		interface PoiGroup {

			@XPath("@groupLabel")
			String getGroupLabel();

			@XPath("pois")
			Poi[] getPois();
		}

		interface Poi {

			@XPath("@id")
			int getId();

			@XPath("@name")
			String getName();

			@XPath("@address")
			@Nullable
			String getAddress();

			@XPath("@phone")
			@Nullable
			String getPhone();

			@XPath("@floor")
			@Nullable
			String getFloor();

			@XPath("@email")
			@Nullable
			String getEmail();

			@XPath("@fax")
			@Nullable
			String getFax();

			@XPath("@openingHours")
			@Nullable
			String getOpeningHours();

			@XPath("@itinerary")
			@Nullable
			String getItinerary();

			@XPath("@coordinates")
			String getCoordinates();

			@XPath("@lat")
			String getLatitude();

			@XPath("@lng")
			String getLongitude();

			@XPath("@lat")
			double getLatAsDouble();

			@XPath("@lng")
			double getLngAsDouble();

			@XPath("@url")
			@Nullable
			String getUrl();

			@XPath("@image")
			@Nullable
			String getImage();

			@XPath("@imageWidth")
			int getImageWidth();

			@XPath("@imageHeight")
			int getImageHeight();

			@XPath("@markerType")
			String getMarkerType();

			@XPath("@markerIndex")
			String getMarkerIndex();
		}

		@XPath("groups/pois/@email")
		@Nullable
		String getEmails();
	}
}
