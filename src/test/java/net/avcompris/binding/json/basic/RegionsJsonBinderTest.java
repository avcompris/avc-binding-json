package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.basic.RegionsJsonBinderTest.Regions.Region;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

public class RegionsJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Before
	public void setUp() throws Exception {

		final Object json = parse(new File("src/test/json/002-regions.json"));

		final JsonBinder binder = new DomJsonBinder();

		regions = binder.bind(json, Regions.class);
	}

	private Regions regions;

	@Test
	public void testRootElementName() throws Exception {

		assertEquals("json", regions.getRootElementName());
	}
	
	@Test
	public void testSizeOfRegions() throws Exception {

		assertEquals(3, regions.sizeOfRegions());
	}

	@Test
	public void testRegionArray() throws Exception {

		final Region region0 = regions.getRegions()[0];

		assertEquals("ile_de_france", region0.getId());
		assertEquals("Île de France", region0.getLabel());
		assertEquals("http://m.univmobile.fr", region0.getUrl());

		final Region region1 = regions.getRegions()[1];

		assertEquals("bretagne", region1.getId());
		assertEquals("Bretagne", region1.getLabel());

		final Region region2 = regions.getRegions()[2];

		assertEquals("Limousin/Poitou-Charentes", region2.getLabel());
	}

	@Test
	public void testGetRegionById_Île_de_France() throws Exception {

		final Region region = regions.getRegionById("ile_de_france");

		assertEquals("ile_de_france", region.getId());
		assertEquals("Île de France", region.getLabel());
		assertEquals("http://m.univmobile.fr", region.getUrl());
	}

	@Test
	public void testGetRegionById_Bretagne() throws Exception {

		final Region region = regions.getRegionById("bretagne");

		assertEquals("bretagne", region.getId());
		assertEquals("Bretagne", region.getLabel());
	}

	@XPath("/*")
	public interface Regions {

		@XPath("region")
		Region[] getRegions();

		int sizeOfRegions();

		@XPath("region[@id = $arg0]")
		Region getRegionById(String id);

		interface Region {

			@XPath("@id")
			String getId();

			@XPath("@label")
			String getLabel();

			@XPath("@url")
			String getUrl();
		}
		
		@XPath("name(.)")
		String getRootElementName();
	}
}
