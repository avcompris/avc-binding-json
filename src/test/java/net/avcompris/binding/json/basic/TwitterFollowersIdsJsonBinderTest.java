package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

public class TwitterFollowersIdsJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Before
	public void setUp() throws Exception {

		final Object json = parse(new File(
				"src/test/json/007-twitter-followersIds.json"));

		final JsonBinder binder = new DomJsonBinder();

		followersIds = binder.bind(json, FollowersIds.class);
	}

	private FollowersIds followersIds;

	@Test
	public void test_intArray() {

		assertEquals(20, followersIds.getIds().length);

		assertEquals(78294483, followersIds.getIds()[0]);
	}

	@Test
	public void test_stringArray() {

		assertEquals(20, followersIds.getIdsAsStrings().length);

		assertEquals("78294483", followersIds.getIdsAsStrings()[0]);
	}

	@XPath("/*")
	public interface FollowersIds {

		@XPath("ids")
		int[] getIds();

		@XPath("ids")
		String[] getIdsAsStrings();
	}
}
