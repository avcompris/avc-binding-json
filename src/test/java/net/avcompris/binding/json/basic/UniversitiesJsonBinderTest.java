package net.avcompris.binding.json.basic;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.json.JsonBinder;
import net.avcompris.binding.json.basic.UniversitiesJsonBinderTest.Universities.University;
import net.avcompris.binding.json.impl.DomJsonBinder;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

import com.avcompris.common.annotation.Nullable;

public class UniversitiesJsonBinderTest {

	private static Object parse(final File jsonFile) throws IOException {

		return JSONValue.parse(FileUtils.readFileToString(jsonFile, UTF_8));
	}

	@Before
	public void setUp() throws Exception {

		final Object json = parse(new File(
				"src/test/json/004-listUniversities_ile_de_france.json"));

		final JsonBinder binder = new DomJsonBinder();

		universities = binder.bind(json, Universities.class);
	}

	private Universities universities;

	@Test
	public void testSizeOfUniversities() throws Exception {

		assertEquals(18, universities.sizeOfUniversities());
	}

	@Test
	public void testUniversityParisNord() throws Exception {

		final University university = universities.getById("paris13");

		assertEquals("paris13", university.getId());
		assertEquals("Paris Nord", university.getTitle());
	}

	@Test
	public void testUniversityParisNord_nullableField() throws Exception {

		final University university = universities.getById("paris13");

		assertTrue(university.isNullTata());
		assertTrue(university.isNullToto());

		assertNotNull(university.getTata());
		assertNull(university.getToto());
	}

	@XPath("/*")
	public interface Universities {

		@XPath("universities")
		int sizeOfUniversities();

		@XPath("universities[@id = $arg0]")
		University getById(String id);

		interface University {

			@XPath("@id")
			String getId();

			@XPath("@title")
			String getTitle();
			
			@XPath("@toto")
			@Nullable
			String getToto();
			
			boolean isNullToto();
			
			@XPath("@tata")
			String getTata();
			
			boolean isNullTata();
		}
	}
}
