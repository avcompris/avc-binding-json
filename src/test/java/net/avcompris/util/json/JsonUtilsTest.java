package net.avcompris.util.json;

import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import javax.annotation.Nullable;

import org.junit.Test;

public class JsonUtilsTest {

	@Test
	public void testParseJSON_simple() throws Exception {

		final MyPerson p = JsonUtils.parseJSON( //
				"{\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"," //
						+ "\"age\":69," //
						+ "\"enabled\":true}",
				MyPerson.class);

		assertEquals("William", p.getFirstName());
		assertEquals("Blake", p.getLastName());
		assertEquals(69, p.getAge());
		assertEquals(true, p.isEnabled());

		assertEquals( //
				"{\"age\":69," //
						+ "\"enabled\":true," //
						+ "\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"}",
				p.toString());

		assertNotEquals(0, p.hashCode());

		assertEquals(p.hashCode(), p.hashCode());

		assertNotNull(p.getClass());

		assertTrue(p.equals(p));
		assertFalse(p.equals(null));
		assertEquals(p, p);
	}

	@Test
	public void testParseJSON_nullInteger() throws Exception {

		final MyPerson p = JsonUtils.parseJSON( //
				"{\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"," //
						+ "\"age\":69," //
						+ "\"enabled\":true}",
				MyPerson.class);

		assertNull(p.getUnknownNumber());
	}

	@Test
	public void testParseJSON_nonNullInteger() throws Exception {

		final MyPerson p = JsonUtils.parseJSON( //
				"{\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"," //
						+ "\"age\":69," //
						+ "\"enabled\":true,"//
						+ "\"unknownNumber\":129}",
				MyPerson.class);

		assertEquals(129, p.getUnknownNumber().intValue());
	}

	private interface MyPerson {

		String getFirstName();

		String getLastName();

		int getAge();

		boolean isEnabled();

		@Nullable
		Integer getUnknownNumber();
	}

	private interface MyData {

		byte[] getPayload();
	}

	@Test
	public void testParseJSON_withArray() throws Exception {

		final MyTeam t = JsonUtils.parseJSON( //
				"{\"people\":[" //
						+ "{\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"," //
						+ "\"age\":69," //
						+ "\"enabled\":true},"//
						+ "{\"firstName\":\"Emily\"," //
						+ "\"lastName\":\"Dickinson\"," //
						+ "\"age\":55," //
						+ "\"enabled\":false}" //
						+ "]}",
				MyTeam.class);

		assertEquals(2, t.getPeople().length);

		final MyPerson p0 = t.getPeople()[0];
		final MyPerson p1 = t.getPeople()[1];

		assertEquals("William", p0.getFirstName());
		assertEquals("Blake", p0.getLastName());
		assertEquals(69, p0.getAge());
		assertEquals(true, p0.isEnabled());

		assertEquals("Emily", p1.getFirstName());
		assertEquals("Dickinson", p1.getLastName());
		assertEquals(55, p1.getAge());
		assertEquals(false, p1.isEnabled());

		assertEquals( //
				"{\"people\":[" //
						+ "{\"age\":69," //
						+ "\"enabled\":true," //
						+ "\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"}," //
						+ "{\"age\":55," //
						+ "\"enabled\":false," //
						+ "\"firstName\":\"Emily\"," //
						+ "\"lastName\":\"Dickinson\"}" //
						+ "]}",
				t.toString());

		assertNotEquals(0, t.hashCode());

		assertEquals(t.hashCode(), t.hashCode());

		assertNotNull(t.getClass());

		assertTrue(t.equals(t));
		assertFalse(t.equals(null));
		assertEquals(t, t);
	}

	private interface MyTeam {

		MyPerson[] getPeople();
	}

	@Test
	public void testParseJSONArray() throws Exception {

		final MyPerson[] p = JsonUtils.parseJSON( //
				"[" //
						+ "{\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"," //
						+ "\"age\":69," //
						+ "\"enabled\":true},"//
						+ "{\"firstName\":\"Emily\"," //
						+ "\"lastName\":\"Dickinson\"," //
						+ "\"age\":55," //
						+ "\"enabled\":false}" //
						+ "]",
				MyPerson[].class);

		assertEquals(2, p.length);

		final MyPerson p0 = p[0];
		final MyPerson p1 = p[1];

		assertEquals("William", p0.getFirstName());
		assertEquals("Blake", p0.getLastName());
		assertEquals(69, p0.getAge());
		assertEquals(true, p0.isEnabled());

		assertEquals("Emily", p1.getFirstName());
		assertEquals("Dickinson", p1.getLastName());
		assertEquals(55, p1.getAge());
		assertEquals(false, p1.isEnabled());
	}

	@Test
	public void testParseByteArray() throws Exception {

		final MyData d = JsonUtils.parseJSON( //
				"{\"payload\":\"Y29jbw==\"}", MyData.class);

		assertEquals(4, d.getPayload().length);
		assertEquals((byte) 99 /* c */, d.getPayload()[0]);
		assertEquals((byte) 111 /* o */, d.getPayload()[1]);
		assertEquals((byte) 99 /* c */, d.getPayload()[2]);
		assertEquals((byte) 111 /* o */, d.getPayload()[3]);
		assertArrayEquals("coco".getBytes(UTF_8), d.getPayload());
	}

	private interface MyBook {

		String getTitle();

		MyPerson getAuthor();
	}

	@Test
	public void testParseSubJSON() throws Exception {

		final MyBook b = JsonUtils.parseJSON( //
				"{\"title\":\"Europe: a Prophecy\"," //
						+ "\"author\"{\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"," //
						+ "\"age\":69," //
						+ "\"enabled\":true}}",
				MyBook.class);

		assertEquals("Europe: a Prophecy", b.getTitle());

		final MyPerson p = b.getAuthor();

		assertEquals("William", p.getFirstName());
		assertEquals("Blake", p.getLastName());
		assertEquals(69, p.getAge());
		assertEquals(true, p.isEnabled());

		assertEquals( //
				"{\"age\":69," //
						+ "\"enabled\":true," //
						+ "\"firstName\":\"William\"," //
						+ "\"lastName\":\"Blake\"}",
				p.toString());

		assertNotEquals(0, p.hashCode());

		assertEquals(p.hashCode(), p.hashCode());

		assertNotNull(p.getClass());

		assertTrue(p.equals(p));
		assertFalse(p.equals(null));
		assertEquals(p, p);
	}

	@Test
	public void testParseJSON_enum() throws Exception {

		final MyResult r = JsonUtils.parseJSON( //
				"{\"status\":\"SUCCESS\"}", MyResult.class);

		assertSame(MyResult.Status.SUCCESS, r.getStatus());
	}

	@Test
	public void testParseJSON_enumNull() throws Exception {

		final MyResult r = JsonUtils.parseJSON( //
				"{\"status\":null}", MyResult.class);

		assertNull(r.getStatus());
	}

	private interface MyResult {

		enum Status {

			SUCCESS, FAILURE, ERROR, CANCELLED;
		}

		Status getStatus();
	}
}
