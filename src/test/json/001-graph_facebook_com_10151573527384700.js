{
   "id": "10151573527384700",
   "from": {
      "id": "99206759699",
      "category": "Author",
      "name": "Frans de Waal - Public Page"
   },
   "name": "DEBATE\r\n\r\nCatching up with Amy Parish (former student of Sarah Hrdy and myself, specialist in bonobo power & sex) before our \"debate\" about my book for a packed house in the Central Library of Los Angeles.",
   "picture": "http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/524852_10151573527384700_1749035228_s.jpg",
   "source": "http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/s720x720/524852_10151573527384700_1749035228_n.jpg",
   "height": 435,
   "width": 720,
   "images": [
      {
         "height": 774,
         "width": 1280,
         "source": "http://sphotos-a.ak.fbcdn.net/hphotos-ak-frc1/914024_10151573527384700_1749035228_o.jpg"
      },
      {
         "height": 580,
         "width": 960,
         "source": "http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/524852_10151573527384700_1749035228_n.jpg"
      },
      {
         "height": 435,
         "width": 720,
         "source": "http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/s720x720/524852_10151573527384700_1749035228_n.jpg"
      },
      {
         "height": 362,
         "width": 600,
         "source": "http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/s600x600/524852_10151573527384700_1749035228_n.jpg"
      },
      {
         "height": 290,
         "width": 480,
         "source": "http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/s480x480/524852_10151573527384700_1749035228_n.jpg"
      },
      {
         "height": 193,
         "width": 320,
         "source": "http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/s320x320/524852_10151573527384700_1749035228_n.jpg"
      },
      {
         "height": 108,
         "width": 180,
         "source": "http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/524852_10151573527384700_1749035228_a.jpg"
      },
      {
         "height": 78,
         "width": 130,
         "source": "http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/524852_10151573527384700_1749035228_s.jpg"
      },
      {
         "height": 78,
         "width": 130,
         "source": "http://photos-a.ak.fbcdn.net/hphotos-ak-ash3/s75x225/524852_10151573527384700_1749035228_s.jpg"
      }
   ],
   "link": "http://www.facebook.com/photo.php?fbid=10151573527384700&set=a.138830229699.109404.99206759699&type=1",
   "icon": "http://static.ak.fbcdn.net/rsrc.php/v2/yz/r/StEh3RhPvjk.gif",
   "created_time": "2013-04-18T14:45:27+0000",
   "updated_time": "2013-04-18T14:45:27+0000",
   "tags": {
      "data": [
         {
            "id": "2613722",
            "name": "Frans de Waal",
            "x": 70.512820512821,
            "y": 22.811671087533,
            "created_time": "2013-04-18T14:46:01+0000"
         }
      ],
      "paging": {
         "next": "https://graph.facebook.com/10151573527384700/tags?limit=5000&offset=5000&__after_id=2613722"
      }
   },
   "comments": {
      "data": [
         {
            "id": "10151573527384700_278919320",
            "from": {
               "name": "Julie Weinhouse",
               "id": "547047780"
            },
            "message": "Great talk last night!",
            "can_remove": false,
            "created_time": "2013-04-18T14:51:12+0000",
            "like_count": 0,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278919323",
            "from": {
               "name": "Carol Spr",
               "id": "3415912"
            },
            "message": "She was my professor!",
            "can_remove": false,
            "created_time": "2013-04-18T14:51:54+0000",
            "like_count": 2,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278919369",
            "from": {
               "name": "Marijn Prakke",
               "id": "668185630"
            },
            "message": "I can see you are trying to impress her...",
            "can_remove": false,
            "created_time": "2013-04-18T14:58:52+0000",
            "like_count": 0,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278919377",
            "from": {
               "name": "Steve Ochs",
               "id": "637474038"
            },
            "message": "Great conversation.  What freaked me out was that Frans declared himself a member of the same \"religion\" that I thought I invented for myself a few years ago!  Like me, he is an \"Apathist;\"  we don't know if there is a god and we don't care. Too cool. \nAlso best of luck in Congo to Charity, who I assume sees these posts.",
            "can_remove": false,
            "created_time": "2013-04-18T15:01:24+0000",
            "like_count": 2,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278919452",
            "from": {
               "name": "Terri Farrugia",
               "id": "100004583466973"
            },
            "message": "I like that...'Apathist'. Good one!",
            "can_remove": false,
            "created_time": "2013-04-18T15:19:00+0000",
            "like_count": 0,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278919488",
            "from": {
               "name": "Martin Cohen",
               "id": "682985087"
            },
            "message": "Couldn't make it last night, wish I had.",
            "can_remove": false,
            "created_time": "2013-04-18T15:24:06+0000",
            "like_count": 0,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278919848",
            "from": {
               "name": "Marian Brickner",
               "id": "100003600593069"
            },
            "message": "Love it. Wpuld have loved to hear the debate!",
            "can_remove": false,
            "created_time": "2013-04-18T16:33:33+0000",
            "like_count": 0,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278920371",
            "from": {
               "name": "Linda Wilshusen",
               "id": "1314372131"
            },
            "message": "would love to see a video...what was the 'debate' aspect??",
            "can_remove": false,
            "created_time": "2013-04-18T17:50:38+0000",
            "like_count": 0,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278921499",
            "from": {
               "id": "99206759699",
               "category": "Author",
               "name": "Frans de Waal - Public Page"
            },
            "message": "There will be an audio podcast. The series is called ALOUD, and they will put it on their website.",
            "can_remove": false,
            "created_time": "2013-04-18T20:59:19+0000",
            "like_count": 1,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278921635",
            "from": {
               "name": "Elaine Marie Agut",
               "id": "1537348282"
            },
            "message": "Cool picture with the double image of you and your former student.",
            "can_remove": false,
            "created_time": "2013-04-18T21:18:28+0000",
            "like_count": 0,
            "user_likes": false
         },
         {
            "id": "10151573527384700_278922071",
            "from": {
               "name": "Phil Stokoe",
               "id": "100000101722153"
            },
            "message": "Is she on Twitter?",
            "can_remove": false,
            "created_time": "2013-04-18T22:59:51+0000",
            "like_count": 0,
            "user_likes": false
         }
      ],
      "paging": {
         "next": "https://graph.facebook.com/10151573527384700/comments?limit=25&offset=25&__after_id=10151573527384700_278922071"
      }
   },
   "likes": {
      "data": [
         {
            "id": "1087421746",
            "name": "LouAnne M. Armstrong"
         },
         {
            "id": "100000109614621",
            "name": "Ute Strech-Jurk"
         },
         {
            "id": "1386373729",
            "name": "Wim de Waal"
         },
         {
            "id": "100001523061652",
            "name": "Domaine de Soulisse"
         },
         {
            "id": "100000679467528",
            "name": "Carlos Contreras"
         },
         {
            "id": "1598956839",
            "name": "Lesleigh Luttrell"
         },
         {
            "id": "582957575",
            "name": "Pepper Elvin"
         },
         {
            "id": "511101420",
            "name": "Alex Anne-Sophie Liberman"
         },
         {
            "id": "21006125",
            "name": "Mavaddat Javid"
         },
         {
            "id": "100000453679721",
            "name": "Mick Vernon"
         },
         {
            "id": "100001571587909",
            "name": "Imbi Taniel"
         },
         {
            "id": "100000226934018",
            "name": "Stella Savenije"
         },
         {
            "id": "792890561",
            "name": "Sian Evans"
         },
         {
            "id": "3408752",
            "name": "Stephanie d'Arc Taylor"
         },
         {
            "id": "527570602",
            "name": "Anita Kania"
         },
         {
            "id": "55003944",
            "name": "Ben Bradley"
         },
         {
            "id": "100001714657585",
            "name": "Susana Mendes"
         },
         {
            "id": "535565578",
            "name": "Richard Prins"
         },
         {
            "id": "100001421604886",
            "name": "Els van Stokkum"
         },
         {
            "id": "100001125725009",
            "name": "Stefano Di Criscio"
         },
         {
            "id": "100000023268530",
            "name": "Miguel Lessa"
         },
         {
            "id": "1179541920",
            "name": "Wendy Dirks"
         },
         {
            "id": "721550548",
            "name": "Helga Vierich"
         },
         {
            "id": "1565568006",
            "name": "Jennifer Verdolin"
         },
         {
            "id": "100000352014734",
            "name": "Tim Kavanagh"
         }
      ],
      "paging": {
         "next": "https://graph.facebook.com/10151573527384700/likes?limit=25&offset=25&__after_id=100000352014734"
      }
   }
}